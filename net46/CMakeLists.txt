﻿# CMakeList.txt : CMake project for latrencygg-csharp-cmake, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.8)

project(LatencyGG VERSION 0.2.4 LANGUAGES CSharp)

include(CSharpUtilities)

# Add source to this project's library
add_library(LatencyGG-net46 SHARED
    ../src/LatencyGG.cs
    ../src/DataPing.cs
    ../src/DataPingWire.cs
    ../src/Stats.cs
    ../src/TimestampMS.cs
    ./Properties/AssemblyInfo.cs)

set_property(TARGET LatencyGG-net46 PROPERTY DOTNET_TARGET_FRAMEWORK "net46")
set(CMAKE_CSharp_FLAGS "/langversion:latest")

set_property(TARGET LatencyGG-net46 PROPERTY VS_DOTNET_REFERENCES
    "Microsoft.CSharp"
    "PresentationCore"
    "PresentationFramework"
    "System"
    "System.Core"
    "System.Data"
    "System.Data.DataSetExtensions"
    "System.Net.Http")

install(TARGETS LatencyGG-net46 COMPONENT net46)
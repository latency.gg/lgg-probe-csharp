﻿/* This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details. */

using System;

namespace LatencyGG
{
    class TimestampMS { 
        public static UInt64 Now()
        {
            return Convert.ToUInt64(DateTimeOffset.Now.UtcDateTime.Ticks / 10000L - 62135596800000L);
        }
    }

}

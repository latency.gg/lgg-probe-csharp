﻿/* This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details. */

using System;
using System.Runtime.InteropServices;

namespace LatencyGG
{ 

    public enum DataPingResponseType: Int32
    {
        eNull = 0,
        eInit,
        eNext,
        eFinal = 255
    }

    public enum DataPingRequestType: Int32
    {
        eInit = 1,
        eSecond,
        eFinal = 255
    }

    class DataPingRequestContainer
    {
        private Int32 mVersion;
        private DataPingRequestType mType;
        private UInt16 mSeq;
        private String mIdent;
        private String mToken;
        public UInt64 mTimestamp;
        public DataPingRequestContainer(Int32 aVersion, UInt16 aSeq, String aIdent, String aToken, UInt64 aTimestamp)
        {
            mVersion = aVersion;
            mSeq = aSeq;
            mIdent = aIdent;
            mToken = aToken;
            mTimestamp = aTimestamp;
            mType = DataPingRequestType.eInit;
        }
        public void SetType(DataPingRequestType aType) 
        {
            mType = aType;
        }
        public Byte[] Serialize()
        {
            Byte[] output;
            switch (mVersion)
            {
                case 2:
                    {
                        DataPingRequestV2 outPacket = new DataPingRequestV2();
                        outPacket.mVersion = 2;
                        outPacket.mIdent = mIdent;
                        outPacket.mSeq = mSeq;
                        outPacket.mTimestamp = mTimestamp;
                        outPacket.mType = mType;
                        output = outPacket.Serialize();
                        break;
                    }
                default:
                case 3:
                    {
                        DataPingRequestV3 outPacket = new DataPingRequestV3();
                        outPacket.mVersion = 3;
                        outPacket.mToken = mToken;
                        outPacket.mSeq = mSeq;
                        outPacket.mTimestamp = mTimestamp;
                        outPacket.mType = mType;
                        output = outPacket.Serialize();
                        break;
                    }
            }
            return output;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct DataPingRequestV3
    {
        public Int32 mVersion;
        public DataPingRequestType mType;
        public UInt16 mSeq;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
        public String UNUSED1;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 160)]
        public String mToken;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public String UNUSED2;
        public UInt64 mTimestamp;

        public Byte[] Serialize()
        {
            UInt16 buffSize = 192;
            Byte[] output = new Byte[buffSize];
            IntPtr handle = Marshal.AllocHGlobal(buffSize);
            Marshal.StructureToPtr(this, handle, false);
            Marshal.Copy(handle, output, 0, buffSize);
            Marshal.FreeHGlobal(handle);
            return output;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct DataPingRequestV2
    {
        public Int32 mVersion;
        public DataPingRequestType mType;
        public UInt16 mSeq;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 89)]
        public String mIdent;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
        public String UNUSED;
        public UInt64 mTimestamp;

        public Byte[] Serialize()
        {
            UInt16 buffSize = 112;
            Byte[] output = new Byte[buffSize];
            IntPtr handle = Marshal.AllocHGlobal(buffSize);
            Marshal.StructureToPtr(this, handle, false);
            Marshal.Copy(handle, output, 0, buffSize);
            Marshal.FreeHGlobal(handle);
            return output;
        }
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    struct DataPingResponse
    {
        public Int32 mVersion;
        public DataPingResponseType mType;
        public UInt16 mSeq;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 89)]
        public String mSig;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 5)]
        public String UNUSED;
        public UInt64 mTimestamp;

        public static DataPingResponse Deserialize(Byte[] aData)
        {
            UInt16 buffSize = 112;
            DataPingResponse dataPingResponse;
            IntPtr handle = Marshal.AllocHGlobal(buffSize);
            Marshal.Copy(aData, 0, handle, buffSize);
            dataPingResponse = (DataPingResponse)Marshal.PtrToStructure(handle, typeof(DataPingResponse));
            return dataPingResponse;
        }
    }
}

﻿/* This program is free software. It comes without any warranty, to
     * the extent permitted by applicable law. You can redistribute it
     * and/or modify it under the terms of the Do What The Fuck You Want
     * To Public License, Version 2, as published by Sam Hocevar. See
     * http://www.wtfpl.net/ for more details. */

using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;

static class KvpExtensions
{
    public static void Deconstruct<TKey, TValue>(
        this KeyValuePair<TKey, TValue> kvp,
        out TKey key,
        out TValue value)
    {
        key = kvp.Key;
        value = kvp.Value;
    }
}

namespace LatencyGG
{
    public enum AF: UInt16
    {
        eINET = 2,
        eINET6 = 23
    }

    public class LatencyGG
    {
        AF mAF;
        Dictionary<String, DataPing> mPings;
        UInt32 mTimeout;
        UInt64 mtimeStarted;
        bool mKilled;
        public LatencyGG() {
            mtimeStarted = TimestampMS.Now();
            mAF = AF.eINET;
            mPings = new Dictionary<String, DataPing>();
            mTimeout = 2000;
            mKilled = false;
        }

        public LatencyGG(AF aAf, UInt32 aTimeout)
        {
            mtimeStarted = TimestampMS.Now();
            mAF = aAf;
            mPings = new Dictionary<String, DataPing>();
            mTimeout = aTimeout;
            mKilled = false;
        }
        public void Add(DataPing aPing)
        {
            mPings[aPing.mTargetIp] = aPing;
        }

        public List<String> GetKeys()
        {
            List<String> output = new List<string>(mPings.Keys);
            return output;
        }

        public int Length()
        {
            return mPings.Count;
        }

        public DataPing this[String aKey]
        {
            get
            {
                return mPings[aKey];
            }
        }

        public void Run()
        {
            Socket s = new Socket((AddressFamily)mAF, SocketType.Dgram, ProtocolType.Udp);
            s.ReceiveBufferSize = 256;
            foreach ((String aKey, DataPing ping) in mPings)
            {
                IPAddress targetIP = IPAddress.Parse(aKey);
                IPEndPoint endPoint;
                if (mAF == AF.eINET) { endPoint = new IPEndPoint(targetIP, 9998); }
                else { endPoint = new IPEndPoint(targetIP, 9999); }
                s.SendTo(ping.GeneratePacket(), endPoint);
            }
            Byte[] bytesReceived = new Byte[256];
            while (IsRunnable())
            {
                bytesReceived = new Byte[113];
                IPEndPoint sender = new IPEndPoint(IPAddress.Any, 0);
                EndPoint senderRemote = (EndPoint)sender;
                if (s.Available < 112)
                {
                    Thread.SpinWait(1);
                    continue;
                }
                int bytesRecv = s.ReceiveFrom(bytesReceived, ref senderRemote);
                sender = (IPEndPoint)senderRemote;
                DataPing ping = mPings[sender.Address.ToString()];
                ping.Update(bytesReceived, bytesRecv);
                if (!ping.IsComplete() && !ping.IsDead())
                {
                    s.SendTo(ping.GeneratePacket(), sender);
                }
                mPings[sender.Address.ToString()] = ping;
            }
            if (TimestampMS.Now() - mtimeStarted >= mTimeout)
            {
                Kill();
            }
            List<String> dead_targets = new List<string>();
            foreach ((String aKey, DataPing ping) in mPings)
            {
                if (!ping.IsComplete())
                {
                    dead_targets.Add(aKey);
                }
            }
            foreach (String target in dead_targets)
            {
                mPings.Remove(target);
            }
        }

        public void Kill() 
        {
            mKilled = true;
            foreach ((String aKey, DataPing ping) in mPings)
            {
                if (!ping.IsComplete())
                {
                    ping.Kill();
                }
            }
        }

        public bool IsComplete() 
        {
            if (Length() == 0)
            {
                return false;
            }
            foreach( (String aKey, DataPing ping) in mPings)
            {
                if (!ping.IsComplete())
                {
                    return false;
                }
            }
            return true;
        }

        public bool IsRunnable() 
        {
            if (TimestampMS.Now() - mtimeStarted >= mTimeout)
            {
                return false;
            }
            if (mKilled)
            {
                return false;
            }
            foreach ((String aKey, DataPing ping) in mPings)
            {
                if (!ping.IsComplete() && !ping.IsEmpty() && !ping.IsDead())
                {
                    return true;
                }
            }
            return false;
        }

    }
}
